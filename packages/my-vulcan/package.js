Package.describe({
  name: 'my-vulcan',
});

/* Блок api.use визначає залежності нашого пакунка: 
vulcan: core, vulcan: forms і vulcan: accounts, пакет, 
який надає набір компонентів React для керування входом і реєстрацією.
 */

Package.onUse(function (api) {

  api.use([
    'vulcan:core',
    'vulcan:forms',
    'vulcan:accounts',
  ]);

  api.addFiles('lib/stylesheets/bootstrap.min.css', 'client');

  api.mainModule('lib/server/main.js', 'server');
  api.mainModule('lib/client/main.js', 'client');

});