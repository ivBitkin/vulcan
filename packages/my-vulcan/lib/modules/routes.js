import { addRoute } from 'meteor/vulcan:core';

import CountriesList from '../components/countries/CountriesList.jsx';

/* addRoute -  обгортка за маршрутизатора React */

addRoute({ name: 'countries', path: '/', component: CountriesList });